# Workspace Theme

This module allows you to specify a specific installed theme that should be used when a workspace is active.

Why? If you are working on a site redesign, this allows you to preview the theme in your live site without it being visible to regular users. Users just need to have access to switch to that workspace and they can see the new theme.
