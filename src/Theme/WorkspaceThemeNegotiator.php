<?php declare(strict_types=1);

namespace Drupal\workspace_theme\Theme;

use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\workspaces\WorkspaceManagerInterface;

final class WorkspaceThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * @var \Drupal\workspaces\WorkspaceManagerInterface
   */
  private $workspaceManager;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  public function __construct(WorkspaceManagerInterface $workspaceManager, AdminContext $admin_context) {
    $this->workspaceManager = $workspaceManager;
    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    // Do not change the admin theme.
    if ($this->adminContext->isAdminRoute($route_match->getRouteObject())) {
      return FALSE;
    }
    if ($this->workspaceManager->hasActiveWorkspace()) {
      $workspace = $this->workspaceManager->getActiveWorkspace();
      return !$workspace->get('theme')->isEmpty();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $workspace = $this->workspaceManager->getActiveWorkspace();
    return $workspace->get('theme')->value;
  }

}
