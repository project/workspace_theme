<?php declare(strict_types=1);

namespace Drupal\workspace_theme\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Supports validating all primitive types.
 *
 * @Constraint(
 *   id = "InstalledTheme",
 *   label = @Translation("Installed theme", context = "Validation")
 * )
 */
final class InstalledThemeConstraint extends Constraint {

  public $message = 'The %value theme is not installed.';

}
