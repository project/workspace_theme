<?php declare(strict_types=1);

namespace Drupal\workspace_theme\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class InstalledThemeConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  private $themeHandler;

  /**
   * Constructs a new InstalledThemeConstraintValidator object.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(ThemeHandlerInterface $theme_handler) {
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    assert($value instanceof FieldItemListInterface);
    if ($value->isEmpty()) {
      return;
    }
    $theme_name = $value->value;
    if (!$this->themeHandler->themeExists($theme_name)) {
      $this->context->addViolation($constraint->message, [
        '%value' => $theme_name
      ]);
    }
  }

}
