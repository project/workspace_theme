<?php declare(strict_types=1);

namespace Drupal\Tests\workspace_theme\Kernel;

use Drupal\Core\Routing\RouteMatch;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\workspaces\Entity\Workspace;
use Symfony\Component\Routing\Route;

/**
 * @group workspace_theme
 * @coversDefaultClass \Drupal\workspace_theme\Theme\WorkspaceThemeNegotiator
 */
final class WorkspaceThemeNegotiatorTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'path_alias',
    'workspaces',
    'workspace_theme',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('workspace');
    $this->installSchema('workspaces', ['workspace_association']);
    $admin_user = $this->createUser([], [
      'create workspace',
      'edit any workspace',
      'view any workspace',
    ]);
    $this->container->get('current_user')->setAccount($admin_user);
  }

  /**
   * Tests that the negotiator applies when appropriate.
   *
   * @param \Drupal\Core\Routing\RouteMatch $route_match
   *   The test route match.
   * @param string $workspace_theme
   *   The test workspace theme value.
   * @param bool $is_workspace_active
   *   Is the test workspace active.
   * @param bool $expected_result
   *   The expected `applies` result.
   *
   * @covers ::applies
   * @dataProvider appliesDataProvider
   */
  public function testApplies(RouteMatch $route_match, string $workspace_theme, bool $is_workspace_active, bool $expected_result) {
    $workspace = Workspace::create([
      'id' => 'foo',
      'label' => 'Foo',
      'theme' => $workspace_theme,
    ]);

    if ($is_workspace_active) {
      $this->container->get('workspaces.manager')->setActiveWorkspace($workspace);
    }

    $sut = $this->container->get('workspace_theme.theme_negotiator');
    self::assertEquals($expected_result, $sut->applies($route_match));
  }

  /**
   * Tests the determined active theme.
   *
   * @covers ::determineActiveTheme
   */
  public function testDetermineActiveTheme() {
    $workspace = Workspace::create([
      'id' => 'foo',
      'label' => 'Foo',
      'theme' => 'stark',
    ]);
    $this->container->get('workspaces.manager')->setActiveWorkspace($workspace);
    $sut = $this->container->get('workspace_theme.theme_negotiator');
    self::assertEquals('stark', $sut->determineActiveTheme(
      new RouteMatch('foo', new Route('/foo', [], [], ['_admin_route' => FALSE]))
    ));
  }

  /**
   * The test data.
   *
   * @return \Generator
   *   The test data.
   */
  public function appliesDataProvider() {
    yield [
      new RouteMatch('foo', new Route('/foo', [], [], ['_admin_route' => TRUE])),
      'stark',
      TRUE,
      FALSE,
    ];
    yield [
      new RouteMatch('foo', new Route('/foo', [], [], ['_admin_route' => FALSE])),
      'stark',
      FALSE,
      FALSE,
    ];
    yield [
      new RouteMatch('foo', new Route('/foo', [], [], ['_admin_route' => FALSE])),
      '',
      TRUE,
      FALSE,
    ];
    yield [
      new RouteMatch('foo', new Route('/foo', [], [], ['_admin_route' => FALSE])),
      'stark',
      TRUE,
      TRUE,
    ];
  }


}
