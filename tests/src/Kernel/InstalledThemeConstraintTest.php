<?php declare(strict_types=1);

namespace Drupal\Tests\workspace_theme\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\workspaces\Entity\Workspace;

/**
 * @group workspace_theme
 */
final class InstalledThemeConstraintTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'path_alias',
    'workspaces',
    'workspace_theme',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('workspace');
    $this->installSchema('workspaces', ['workspace_association']);
    $this->container->get('theme_installer')->install([
      'stark',
    ]);
  }

  /**
   * Tests that the base field is added to the Workspace.
   */
  public function testConstraint() {
    $workspace = Workspace::create([
      'id' => 'foo',
      'label' => 'Foo',
      'theme' => '',
    ]);
    $violations = $workspace->validate();
    self::assertCount(0, $violations, 'Empty theme does not cause a constraint violation');
    $workspace->theme = 'bartik';
    $violations = $workspace->validate();
    self::assertCount(1, $violations, 'Empty theme does not cause a constraint violation');
    $violation = $violations->get(0);
    self::assertEquals('The <em class="placeholder">bartik</em> theme is not installed.', $violation->getMessage());
    $workspace->theme = 'stark';
    $violations = $workspace->validate();
    self::assertCount(0, $violations, 'Installed theme does not cause a constraint violation');
  }

}
