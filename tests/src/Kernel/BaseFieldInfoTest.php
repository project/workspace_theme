<?php declare(strict_types=1);

namespace Drupal\Tests\workspace_theme\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * @group workspace_theme
 */
final class BaseFieldInfoTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'path_alias',
    'workspaces',
    'workspace_theme',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('workspace');
  }

  /**
   * Tests that the base field is added to the Workspace.
   */
  public function testFieldExists() {
    $fields = $this->container->get('entity_field.manager')->getBaseFieldDefinitions('workspace');
    self::assertArrayHasKey('theme', $fields);
  }

}
